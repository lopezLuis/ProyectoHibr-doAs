var express = require('express');
var app = express();

app.post('/authenticate', function(req, res) {
    var username = req.params.username;
    var password = req.params.password;
require('./modelos/login').authenticate(username, password)
    .then(function(login){
        if(login){
            res.json(login);
        }else{
            res.status(404).json({errorMessage:''});
        }
    });
});
// app.post('/api/users', function(req, res) {
//     var user_id = req.body.id;
//     var token = req.body.token;
//     var geo = req.body.geo;

//     res.send(user_id + ' ' + token + ' ' + geo);
// });