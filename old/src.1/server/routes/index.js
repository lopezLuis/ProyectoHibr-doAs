var _ = require('lodash');
var bodyParser = require('body-parser');

module.exports = function (app){
    app.use(bodyParser.json());
    
    app.use(
        '/api/users',
        require('./api/userRoutes')
    );
    
    app.get('/ping', function (req, res, next) {
        res.send('pong');
    });
};

