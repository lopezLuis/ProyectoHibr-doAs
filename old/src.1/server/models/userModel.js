var conn, models;
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = function (db) {
    conn = db.connection;
    models = db.models;
    var api = {
        authenticate: authenticate,
        dataClient: dataClient
    };
    return api;
};

function authenticate(payload){
    return conn.pgm('ESUSU',
 	    [
 	        {name: 'sal', size: 1,},
 	        {name: 'idUser', size: 10},
 	        {name: 'passCli', size: 10}
        ])({
            sal: '0' ,
            idUser: payload.idUser,
            passCli: payload.password
            
        })
        .then(function(result){
            console.log(result);
            return result.sal === '1' ? result : null
        });
}
function dataClient(payload){
    return conn.pgm('CLIPANTPRC',
 	    [
 	        {name: 'idUserCon', size: 10},
 	        {dsCli: 
 	         [
 	             {name: 'id',typeName: 'NUMERIC', precision: 10, scale: 0},
 	             {name: 'username', size: 50},
					{name: 'firstname', size: 50},
					{name: 'secondname',size: 50},
					{name: 'numberClientID',size: 10},
					{name: 'nationality',size: 50},
					{name: 'birthdate',size: 10},
					{name: 'districtName',size: 50},
					{name: 'address',size: 100},
					{name: 'cantonName',size: 50},
					{name: 'provinceName',size: 30},
					{name: 'nameTypeID',size: 45}
					]}
        ])({
            idUserCon: payload.idUserCon,
            dsCli: 
            {
                id:'0',
                username: '0', 
                firstname: '0', 
                secondname: '0',
                numberClientID: '0',
                nationality: '0',
                birthdate: '0',
                districtName: '0',
                address: '0',
                cantonName: '0',
                provinceName: '0',
                nameTypeID: '0'}
        })
        .then(function(result){
            console.log(result);
            return result.dsCli  ? result : null
        });
}
