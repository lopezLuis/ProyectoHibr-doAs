/* Utilizando la Api de Local Storage, podemos guardar y luego recuperar un valor 
 
localStorage.setItem('saludo', 'Bienvenidos a localStorage');
valor = localStorage.getItem('saludo');
 

localStorage['saludo'] = 'Este saludo también es válido';
valor = localStorage['saludo'];
 

localStorage.saludo = 'Y este es mi método favorito';
valor = localStorage.saludo;

ert('Tenemos ' + localStorage.length + ' elementos dentro de Local Storage');
 

localStorage.removeItem('saludo');
 

  for(var i=0, t=localStorage.length; i < t; i++) {
    key = localStorage.key(i);
    alert('Para la clave ' + key + ' el valor es: ' + localStorage[key]);
}
*/
if('localStorage' in window && window['localStorage'] !== null) {
    //alert('Genial, tenemos un navegador decente que soporta LocalStorage');
    var storage = sessionStorage
} else { alert('el navegador no soporta local storage'); 
window.location="home.html";
}

// Existe localStorage?
var storage;
try {
    if (sessionStorage.getItem) {
        storage = sessionStorage;
    }
} catch(e) {
    storage = {};
}

//persona = {nombre: };
//localStorage.user = persona;
//alert(localStorage.autor);
// Esto nos devuelve el texto
//"[object Object]"
// Primero convertimos el objeto en una cadena de texto
 
//localStorage.user = JSON.stringify(persona);

  //alert( localStorage.getItem('user'));
/* Y ahora, al recuperarlo, convertimos el string nuevamente en un objeto */
 

//alert(localStorage.getItem('nombreU'));
//alert(typeof autor);
 hay_cambio = function(e) {
    console.log("Dentro de la clave " + e.key + " el valor anterior era " + e.oldValue + ' y el nuevo es ' + e.newValue);
}
if(window.addEventListener) {
    window.addEventListener('storage', hay_cambio, false);
} else {
    // Hay que soportar IE6, 7 y 8, lo lamento
    window.attachEvent('onstorage', hay_cambio);
}