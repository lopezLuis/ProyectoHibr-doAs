var conn, models;
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = function (db) {
    conn = db.connection;
    models = db.models;
    var api = {
        authenticate: authenticate
    };
    return api;
};

function authenticate(payload){
    return conn.pgm('ESUSU',
 	    [
 	        {name: 'sal', size: 1,},
 	        {name: 'idUser', size: 10},
 	        {name: 'passCli', size: 10}
        ])({
            sal: '0' ,
            idUser: payload.idUser,
            passCli: payload.password
            
        })
        .then(function(result){
            console.log(result);
            return result.sal === '1' ? result : null
        });
}
