var router = require('express').Router();
var models = require('../../models');
router.route('/authenticate')
    .get(
        function (req, res, next) {
            console.log('Logging in...');
            var idUser = req.query.iduser;
            var password = req.query.password;
            models.user.authenticate(
                {
                    idUser: idUser,
                    password: password
                }
            ).then(function(result){
                if(result){
                    console.log('entro...');
                    return res.json(result);
                }else{
                    return res.status(401).json('not Authorized');
                }
            });
        }
    );
        
module.exports = router;

/*
 * https://as400-daiman20.c9.io/api/users/authenticate?iduser=2&password=2
 * https://as400-daiman20.c9.io/api/users/authenticate?idUser=2&password=2
 * iduser = 2
 * password = serdna
 */
