angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
      
        
    .state('login', {
      url: '/page1',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })
        
      
    
      
        
    .state('tabsController.informacioN', {
      url: '/page4',
      views: {
        'tab1': {
          templateUrl: 'templates/informacioN.html',
          controller: 'informacioNCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.transferencias', {
      url: '/page5',
      views: {
        'tab2': {
          templateUrl: 'templates/transferencias.html',
          controller: 'transferenciasCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.consultas', {
      url: '/page6',
      views: {
        'tab3': {
          templateUrl: 'templates/consultas.html',
          controller: 'consultasCtrl'
        }
      }
    })
        
      
    
      
    .state('tabsController', {
      url: '/page3',
      abstract:true,
      templateUrl: 'templates/tabsController.html'
    })
      
    
      
        
    .state('transferencia', {
      url: '/page7',
      templateUrl: 'templates/transferencia.html',
      controller: 'transferenciaCtrl'
    })
        
      
    
      
        
    .state('transferenciaEntreTusCuentas', {
      url: '/page18',
      templateUrl: 'templates/transferenciaEntreTusCuentas.html',
      controller: 'transferenciaEntreTusCuentasCtrl'
    })
        
      
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/page1');

});